import React, { useEffect, useState } from "react";
import "./App.css";
import TodoList from "./components/TodoList";
//j'importe axios pour l'utiliser
import axios from "axios";

//Je crée une variable contenant l'URL de l'API qui est la route qui permet de faire une requete
const API_URL = "https://jsonplaceholder.typicode.com/todos";

function App() {
  // On crée un state qui va contenir les données de l'api
  const [todos, setTodos] = useState([]);

  // On utilise useEffect pour faire une requete sur l'api
  // La fonction dans useEffect est exécutée à chaque fois que le composant est monté dans le DOM
  // Une seule fois dans ce cas
  useEffect(() => {
     // Appel à l'API avec axios.get pour récupérer des données depuis l'URL spécifiée (API_URL)
    axios.get(API_URL).then((response) => {
      // Affichage de la réponse de l'API dans la console pour le débogage
      console.log(response);
       // Mise à jour de l'état 'todos' avec les données récupérées depuis la réponse de l'API
      setTodos(response.data);
           // La fonction 'setTodos' est fournie par le hook 'useState' et elle met à jour l'état 'todos' du composant
    });
  }, []);
  // Le tableau vide en tant que deuxième argument signifie que ce 'useEffect' ne dépend d'aucune variable,
  // et donc il ne sera exécuté qu'une seule fois après le rendu initial du composant

  return (
    <div className="App">
      {/* On passe la props "api" à notre composant TodoList*/}
      <TodoList todos={todos} />
    </div>
  );
}

export default App;
